﻿namespace IpCameraScreenShot
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tb_SN = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_CNC = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tb_FC = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btn_Screenshot = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // tb_SN
            // 
            this.tb_SN.Location = new System.Drawing.Point(73, 62);
            this.tb_SN.Mask = "9999999";
            this.tb_SN.Name = "tb_SN";
            this.tb_SN.Size = new System.Drawing.Size(61, 20);
            this.tb_SN.TabIndex = 77;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 80;
            this.label2.Tag = "FormMain";
            this.label2.Text = "Seriale";
            // 
            // tb_CNC
            // 
            this.tb_CNC.Location = new System.Drawing.Point(73, 36);
            this.tb_CNC.Mask = "9999/99";
            this.tb_CNC.Name = "tb_CNC";
            this.tb_CNC.Size = new System.Drawing.Size(60, 20);
            this.tb_CNC.TabIndex = 76;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(39, 39);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 79;
            this.label20.Tag = "FormMain";
            this.label20.Text = "CNC";
            // 
            // tb_FC
            // 
            this.tb_FC.Location = new System.Drawing.Point(73, 10);
            this.tb_FC.Mask = "9999/9999";
            this.tb_FC.Name = "tb_FC";
            this.tb_FC.Size = new System.Drawing.Size(60, 20);
            this.tb_FC.TabIndex = 75;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 78;
            this.label19.Tag = "FormMain";
            this.label19.Text = "Flow Chart";
            // 
            // btn_Screenshot
            // 
            this.btn_Screenshot.ImageKey = "camera.png";
            this.btn_Screenshot.ImageList = this.imageList;
            this.btn_Screenshot.Location = new System.Drawing.Point(157, 15);
            this.btn_Screenshot.Name = "btn_Screenshot";
            this.btn_Screenshot.Size = new System.Drawing.Size(60, 60);
            this.btn_Screenshot.TabIndex = 81;
            this.btn_Screenshot.UseVisualStyleBackColor = true;
            this.btn_Screenshot.Click += new System.EventHandler(this.btn_Screenshot_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "camera.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(243, 94);
            this.Controls.Add(this.btn_Screenshot);
            this.Controls.Add(this.tb_SN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_CNC);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.tb_FC);
            this.Controls.Add(this.label19);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "IpCameraScreenShot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox tb_SN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox tb_CNC;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox tb_FC;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btn_Screenshot;
        private System.Windows.Forms.ImageList imageList;
    }
}

