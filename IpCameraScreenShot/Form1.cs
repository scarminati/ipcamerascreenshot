﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

using System.Drawing.Imaging;


namespace IpCameraScreenShot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Screenshot_Click(object sender, EventArgs e)
        {
            if (String.Compare(tb_FC.Text, "    /", false) == 0)
                MessageBox.Show("Inserisci il numero di Flow Chart", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else if (String.Compare(tb_SN.Text, "", false) == 0)
                MessageBox.Show("Inserisci il numero Seriale", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                this.Visible = false;
                Thread.Sleep(500);
                PrintScreen();
            }
        }

        private void PrintScreen()

        {
            Bitmap printscreen = new Bitmap(Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);// Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);

            Graphics graphics = Graphics.FromImage(printscreen as Image);

            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);

            savePrintScreen(printscreen);

        }

        private void savePrintScreen(Bitmap printscreen)
        {
            try
            {
                string fileName = "";
                string[] FCsplit = tb_FC.Text.Split('/');
                string[] CNCsplit = tb_CNC.Text.Split('/');
                if (tb_CNC.Text.Equals("    /"))
                    fileName = @"ScreenShot" + "_SN = " + tb_SN.Text + "_FC = " + FCsplit[0] + " " + FCsplit[1] + "_DATE = " + DateTime.Now.ToString(@"dd\gMM\myy\ahh\hmm\mss\s").Replace("/", "") + ".jpeg";
                else
                    fileName = @"ScreenShot" + "_SN = " + tb_SN.Text + "_FC = " + FCsplit[0] + " " + FCsplit[1] + "_CNC " + CNCsplit[0] + " " + CNCsplit[1] + "_DATE = " + DateTime.Now.ToString(@"dd\gMM\myy\ahh\hmm\mss\s").Replace("/", "") + ".jpeg";
                //salvo in locale il report
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\Reports\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text))
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\Reports\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text);
                printscreen.Save(AppDomain.CurrentDomain.BaseDirectory + @"\Reports\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + fileName);
                if (!Directory.Exists(@"\\ralcosrv2\Public\Test Report\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text))
                    Directory.CreateDirectory(@"\\ralcosrv2\Public\Test Report\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text);
                if (!Directory.Exists(@"\\ralcosrv2\Public\DHR_Collimatori\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text))
                    Directory.CreateDirectory(@"\\ralcosrv2\Public\DHR_Collimatori\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text);
                //Console.WriteLine(@"\ralcosrv2\Public\Test Report\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\"  + fileName);
                File.Copy(AppDomain.CurrentDomain.BaseDirectory + @"\Reports\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + fileName,
                @"\\ralcosrv2\Public\Test Report\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + @"\" + fileName, true);
                File.Copy(AppDomain.CurrentDomain.BaseDirectory + @"\Reports\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + fileName,
                @"\\ralcosrv2\Public\DHR_Collimatori\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + fileName, true);
                DialogResult res = MessageBox.Show("Immagine salvata correttamente", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (res == DialogResult.OK)
                {
                    this.Visible = true;
                    Process.Start(@"\\ralcosrv2\Public\DHR_Collimatori\" + FCsplit[1] + @"\" + FCsplit[0] + @"\" + tb_SN.Text + @"\" + fileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
